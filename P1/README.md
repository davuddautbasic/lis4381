> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

LIS4381 - Mobile Applcation Development

Davud Dautbasic

Project 1 Requirements:

*Sub-Heading:*

1. Create an app that displays information
2. Screenshots of Applications
3. Stuff
4. Bitbucket repository links

#### README.md file should include the following items:

* Screenshots of Application

*Screenshot of Logo:

![picture](images/logo.PNG)

*Screenshot of First screen*:

![picture](images/first.PNG)

*Screenshot of Second screen:
![picture](images/second.PNG)



#### Tutorial Links:


*A3 lis4381 Repository:*
[P1 Repository Link](https://bitbucket.org/davuddautbasic/lis4381/src/master/P3/ "lis4381 Repository")