# LIS 4381

## Davud Dautbasic

### Project 2 Requirements:

*Create an online portfolio that illustrates the skills acquired while working through various projects in LIS4381.*

1. Screenshots of pages
2. Bitbucket Repo Links
3. Link to local LIS4381 Web App

#### README.md file should include the following items:

* Screenshot of petstore table
* Screenshot of Error Page
* Link local LIS4381 Web App
* Bitbucket Repo Links


#### Assignment Screenshots:

*Screenshot of Main Page*:

![Main Page Screenshot](images/index.PNG)

*Screenshot of Failed Validation*:

![Failed Validation Screenshot](images/error.PNG)

![Main Page Screenshot](images/edit.PNG)

*Screenshot of Failed Validation*:

![Failed Validation Screenshot](images/rssfeed.PNG)




#### Tutorial Link and Assignment Link:

*A4 lis4381 Repository:*
[A4 Repository Link](https://bitbucket.org/davuddautbasic/lis4381/src/master/a5/ "lis4381 Repository")
