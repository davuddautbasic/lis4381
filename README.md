# LIS4381
 
## Davud Dautbasic
 
*Course Work Links:*
 
1. [A1 README.md](A1/README.md "My A1 README.md file")
 
    * Installed AMPPS
    * Installed JDK and Java SE
    * Installed Android Studio and created My First App
    * Provided screenshots of all installations
    * Created Bitbucket repo
    * Completed Bitbucket tutorial (bitbucketstationlocations)
    * Provide git command descriptions

2. [A2 README.md](A2/README.md "My A2 README.md file")
    * Created an application with a button
    * Added an image

3. [A3_ README.md](A3_/README.md "My A3 README.md file")
    * Created an application that has a user select their concert ticket, and it calculates a price based on the number of tickets.
    * Has images of an ERD, and screenshots of the app. 

4. [P1 READMEmd](P1/README.md "My P1 README.md file")
    * Created an application that displays my "business" card.
    * Has images of my application and logo. 

5. [A4 README.md](a4/README.md) "My online Portfolio README.md file")
    * Created a local webapage to house all my assignments
    * edited a php file to make a webpage

6. [A5 README.md](a5/README.md) "Petstore Database README.md file")
    * Created a local database that houses information
    * edited a php file to make a webpage that displays the information from the database

4. [P2 READMEmd](p2/README.md "My P2 README.md file")
    * Created an application that displays my "business" card.
    * Has images of my application and logo. 

