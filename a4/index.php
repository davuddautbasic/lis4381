<!DOCTYPE html>
<html lang="en">
<head>
<!--
				 "Time-stamp: <Sun, 05-27-18, 19:34:59 Eastern Daylight Time>"
//-->
<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Online portfolio that includes many different class assignments.">
	<meta name="author" content="Davud Dautbasic">
	<link rel="icon" href="images/favicon.ico">

	<title>LIS4381 - Assignment 4</title>
		<?php include_once("../css/include_css.php"); ?>

<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">


<link rel="stylesheet" href="css/formValidation.min.css"/>

<link href="css/starter-template.css" rel="stylesheet">



</head>
<body>

	<?php include_once("../global/nav.php"); ?>

	<div class="container">
		<div class="starter-template">
			<div class="row">
				<div class="col-sm-8 col-sm-offset-2">
					
					<div class="page-header">
						<?php include_once("global/header.php"); ?>	
					</div>

					<h2>Pet Stores</h2>

						<form id="defaultForm" method="post" class="form-horizontal" action="#">
								<div class="form-group">
									<label class="col-sm-3 control-label">Name:</label>
									<div class="col-sm-5">
										<input type="text" class="form-control" name="name" />
										</div>
								</div>

								<div class="form-group">
									<label class="col-sm-3 control-label">Street:</label>
									<div class="col-sm-5">
										<input type="text" class="form-control" name="street"/>
									</div>
								</div>

								<div class="form-group">
									<label class="col-sm-3 control-label">City:</label>
									<div class="col-sm-5">
										<input type="text" class="form-control" name="city"/>
									</div>
								</div>

									<div class="form-group">
									<label class="col-sm-3 control-label">State:</label>
									<div class="col-sm-5">
										<input type="text" class="form-control" name="state"/>
									</div>
								</div>

								<div class="form-group">
									<label class="col-sm-3 control-label">Zip:</label>
									<div class="col-sm-5">
										<input type="text" class="form-control" name="zip"/>
									</div>
								</div>

								<div class="form-group">
									<label class="col-sm-3 control-label">Phone:</label>
									<div class="col-sm-5">
										<input type="text" class="form-control" name="phone"/>
									</div>
								</div>

								<div class="form-group">
									<label class="col-sm-3 control-label">Email:</label>
									<div class="col-sm-5">
										<input type="text" class="form-control" name="email" />
										</div>
								</div>

								<div class="form-group">
									<label class="col-sm-3 control-label">URL:</label>
									<div class="col-sm-5">
										<input type="text" class="form-control" name="url"/>
									</div>
								</div>

								<div class="form-group">
									<label class="col-sm-3 control-label">YTD Sales:</label>
									<div class="col-sm-5">
										<input type="text" class="form-control" name="ytdsales"/>
									</div>
								</div>

								<div class="form-group">
									<label class="col-sm-3 control-label">Notes:</label>
									<div class="col-sm-5">
										<input type="text" class="form-control" name="notes"/>
									</div>
								</div>

								<div class="form-group">
									<div class="col-sm-9 col-sm-offset-3">
									<button type="submit" class="btn btn-primary" name="signuup" value="Sign up">Submit</button>
										</div>
								</div>
						</form>
					</div>
				</div>

			<?php include_once "global/footer.php"; ?>
			
		</div> <!-- end starter-template -->
 </div> <!-- end container -->

	
<!-- Bootstrap JavaScript
================================================== -->
<!-- Placed at end of document so pages load faster -->

<script src="https://code.jquery.com/jquery-3.0.0.min.js" integrity="sha256-JmvOoLtYsmqlsWxa7mDDSLMwa6dZ9rrIdtrrVyRnDRH0="crossorigin="anonymous"></script>

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

<script type="text/javascript" src="js/formValidation.min.js"></script>

<script type="text/javascript" src="js/bootstrap.min.js"></script>

<script src="js/ie10-viewport-bug-workaround.js"></script>

<?php include_once("../js/include_js.php"); ?>

<script type="text/javascript">
 //See Regular Expressions: http://www.qcitr.com/usefullinks.htm#lesson7
 $(document).ready(function() {

	$('#defaultForm').formValidation({
			message: 'This value is not valid',
			icon: {
					valid: 'fa fa-check',
					invalid: 'fa fa-times',
					validating: 'fa fa-refresh'
			},
			fields: {
					name: {
							validators: {
									notEmpty: {
									 message: 'Name required'
									},
									stringLength: {
											min: 1,
											max: 30,
									 message: 'Name no more than 30 characters'
									},
									regexp: {
										//alphanumeric, hyphens, underscores, and spaces
										//regexp: /^[a-zA-Z0-9\-_\s]+$/,										
										//similar to: (though, \w supports other Unicode characters)
											regexp: /^[\w\-\s]+$/,
										message: 'Name can only contain letters, numbers, hyphens, and underscore'
									},									
							},
					},
// ------------------------------------------------------------------------------------------
					street: {
							validators: {
									notEmpty: {
											message: 'Street required'
									},
									stringLength: {
											min: 1,
											max: 30,
											message: 'Street no more than 30 characters'
									},
									regexp: {
										//street: only letters, numbers, commas, hyphens, space character, and periods
										regexp: /^[a-zA-Z0-9,\-\s\.]+$/,		
									message: 'Street can only contain letters, numbers, commas, hyphens, or periods'
									},									
							},
					},
// --------------------------------------------------------------------------------------------
					city: {
							validators: {
									notEmpty: {
											message: 'City required'
									},
									stringLength: {
											min: 1,
											max: 30,
											message: 'City no more than 30 characters'
									},
									regexp: {
										regexp: /^[a-zA-Z0-9,\-\s\.]+$/,		
									message: 'City can only contain letters, numbers'
									},									
							},
					},
// --------------------------------------------------------------------------------------------
					state: {
							validators: {
									notEmpty: {
											message: 'State required'
									},
									stringLength: {
											min: 2,
											max: 2,
											message: 'State must be two characters'
									},
									regexp: {
										regexp: /^[a-zA-Z]+$/,	
									message: 'State can only contain letters'
									},									
							},
					},
// --------------------------------------------------------------------------------------------
					zip: {
							validators: {
									notEmpty: {
											message: 'Zip required, only numbers'
									},
									stringLength: {
											min: 5,
											max: 9,
											message: 'Zip must be 5, no more than 9 digits'
									},
									regexp: {
										regexp: /^[0-9]+$/,	
									message: 'Zip can only contain numbers'
									},									
							},
					},
// --------------------------------------------------------------------------------------------
					phone: {
							validators: {
									notEmpty: {
											message: 'Phone required, including area code, only numbers'
									},
									stringLength: {
											min: 10,
											max: 10,
											message: 'Phone must be 10 digits'
									},
									regexp: {
										regexp: /^[0-9]+$/,
									message: 'Phone can only contain numbers'
									},									
							},
					},
// --------------------------------------------------------------------------------------------
					email: {
							validators: {
									notEmpty: {
											message: 'Email is required'
									},
									stringLength: {
											min: 1,
											max: 100,
											message: 'Email no more than 100 characters'
									},
									regexp: {
										regexp: /^([a-z0-9_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})$/,
									message: 'Must include valid email'
									},									
							},
					},
// --------------------------------------------------------------------------------------------
					url: {
							validators: {
									notEmpty: {
											message: 'URL required'
									},
									stringLength: {
											min: 1,
											max: 100,
											message: 'Email no more than 100 characters'
									},
									regexp: {
										regexp: /^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/,
									message: 'Must include valid URL'
									},									
							},
					},
// --------------------------------------------------------------------------------------------
					ytdsales: {
							validators: {
									notEmpty: {
											message: 'YTD Sales is required'
									},
									stringLength: {
											min: 1,
											max: 11,
											message: 'YTD Sales can be no more than 10 digits, including decimal point'
									},
									regexp: {
										regexp: /^[0-9\.]+$/,
									message: 'Must include valid URL'
									},									
							},
					},
			}
	});
});
</script>

</body>
</html>
