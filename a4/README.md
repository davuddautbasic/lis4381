> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

LIS4381 - Mobile Applcation Development

Davud Dautbasic

Assignment 4 Requirements:

*Sub-Heading:*

1. Validation
2. Screenshots of Webpage
3. Directories and repositories
4. Bitbucket repository links

#### README.md file should include the following items:

* Screenshots of Application

#### Assignment Links:

*A3 sql code*:
[Main Page](http://localhost/repos/lis4381/index.php "Local Host main page")

#### Assignment Screenshots:

*Failed Validation*:

![picture](images/home.PNG)

*Passed Validation*:

![picture](images/fail.PNG)

*Screenshot of ERD:
![picture](images/pass.PNG)



#### Tutorial Links:


*A4 lis4381 Repository:*
[A4 Repository Link](https://bitbucket.org/davuddautbasic/lis4381/src/master/a4/ "lis4381 Repository")