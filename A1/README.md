> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

LIS4381 - Mobile Applcation Development

Davud Dautbasic

Assignment 2 Requirements:

*Sub-Heading:*

1. Create an app with Image and Button
2. Screenshots of Applications
3. Bitbucket repository links

#### README.md file should include the following items:

* Screenshots of Application


#### Assignment Screenshots:

*Screenshot of 1st page of App:

![picture](images/bruschetta1.PNG)

*Screenshot of 2nd page of App*:

![picture](images/bruschetta2.PNG)



#### Tutorial Links:


*A1 lis4381 Repository:*
[A2 Repository Link](https://bitbucket.org/username/myteamquotes/ "lis4381 Repository")