-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `mydb` ;

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `mydb` DEFAULT CHARACTER SET utf8 ;
USE `mydb` ;

-- -----------------------------------------------------
-- Table `mydb`.`petstore`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`petstore` (
  `pst_id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `pst_name` VARCHAR(30) NOT NULL,
  `pst_street` VARCHAR(30) NOT NULL,
  `pst_city` VARCHAR(30) NOT NULL,
  `pst_state` CHAR(2) NOT NULL,
  `pst_zip` INT(9) UNSIGNED NOT NULL,
  `pst_phone` BIGINT UNSIGNED NOT NULL,
  `pst_email` VARCHAR(100) NOT NULL,
  `pst_url` VARCHAR(100) NOT NULL,
  `pst_ytd_sales` DECIMAL(10,2) NOT NULL,
  `pst_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`pst_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`customer`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`customer` (
  `cus_id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `cus_fname` VARCHAR(15) NOT NULL,
  `cus_lname` VARCHAR(30) NOT NULL,
  `cus_street` VARCHAR(30) NOT NULL,
  `cus_city` VARCHAR(30) NOT NULL,
  `cust_state` CHAR(2) NOT NULL,
  `cus_zip` INT UNSIGNED ZEROFILL NOT NULL,
  `cus_phone` BIGINT UNSIGNED NOT NULL,
  `cus_email` VARCHAR(100) NOT NULL,
  `cus_balance` DECIMAL(6,2) UNSIGNED NOT NULL,
  `cus_total_sales` DECIMAL(6,2) UNSIGNED NOT NULL,
  `cus_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`cus_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`pet`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`pet` (
  `pet_id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `cus_id` SMALLINT UNSIGNED NULL,
  `pet_type` VARCHAR(45) NOT NULL,
  `pet_sex` ENUM('m', 'f') NOT NULL,
  `pet_cost` DECIMAL(6,2) UNSIGNED NOT NULL,
  `pet_price` DECIMAL(6,2) UNSIGNED NOT NULL,
  `pet_age` TINYINT UNSIGNED NOT NULL,
  `pet_color` VARCHAR(30) NOT NULL,
  `pet_vaccine` ENUM('y', 'n') NOT NULL,
  `pet_neuter` ENUM('y', 'n') NOT NULL,
  `pet_notes` VARCHAR(255) NULL,
  INDEX `fk_pet_customer1_idx` (`cus_id` ASC),
  CONSTRAINT `fk_pet_petstore`
    FOREIGN KEY (`pet_id`)
    REFERENCES `mydb`.`petstore` (`pst_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_pet_customer1`
    FOREIGN KEY (`cus_id`)
    REFERENCES `mydb`.`customer` (`cus_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `mydb`.`petstore`
-- -----------------------------------------------------
START TRANSACTION;
USE `mydb`;
INSERT INTO `mydb`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'Claws & Paws', '123 Main St.', 'L.A.', 'CA', 896573665, 8968968965, 'clawsnpaws@aol.com', 'http://clawsnpaws.com', 8755454, 'notes1');
INSERT INTO `mydb`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'Fur Real Friends', '1425 Chase Dr.', 'Chicago', 'IL', 589478541, 7854132654, 'fureal@gmail.com', 'http://furreal.com', 7894651, 'notes2');
INSERT INTO `mydb`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'The Pet Place', '567 Right Drive', 'NY', 'NY', 123465789, 8527419634, 'PetPlace@aol.com', 'http://petplace.com', 1234645, 'notes4');
INSERT INTO `mydb`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'Grrs n Purrs', '890 Franklin Highway', 'Boston', 'MA', 741852638, 7452369852, 'prrgrr@yahoo.com', 'http://prrsngrrs.com', 6547896, 'notes5');
INSERT INTO `mydb`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'Get Pet', '2343 SW 92 ST', 'Philidelphia', 'PA', 345678902, 8765431623, 'GetPets@aim.com', 'http://getpets.com', 6325842, 'notes6');
INSERT INTO `mydb`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'Bark Meow Ribbit', '8574 Roosevelt Way', 'Washington D.C.', 'VA', 458474561, 6651225484, 'BarkMeowRibbit@gmail.com', 'http://BMR.org', 3282834, 'notes7');
INSERT INTO `mydb`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'All About Pets', '453 Alley Lane', 'Tallahassee', 'FL', 564849412, 8978987879, 'AllAboutPets@aol.com', 'http://AAP.net', 2342344, 'notes8');
INSERT INTO `mydb`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'Doggie Dancing', '9502 NW 83 ST', 'Phoenix', 'AZ', 515616155, 7987978978, 'DoggieDancing@yahoo.com', 'http://DoggieDancing.com', 4923499, 'notes9');
INSERT INTO `mydb`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'Can\'t Stop the Cuteness', '2340 SW 83rd Ave.', 'Denver', 'CO', 879841134, 5656598941, 'Cute247@aol.com', 'http://cuteallthetime.com', 3423412, 'notes10');
INSERT INTO `mydb`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'Beagle Bros', '811 4th ST', 'Miami', 'FL', 789465133, 7894561230, 'BeagleBro@yahoo.com', 'http://beaglebros.com', 7894651, NULL);

COMMIT;


-- -----------------------------------------------------
-- Data for table `mydb`.`customer`
-- -----------------------------------------------------
START TRANSACTION;
USE `mydb`;
INSERT INTO `mydb`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cust_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Bobby', 'Sue', '19567 Rutheford Cir.', 'Charleston', 'WV', 32304, 8809809080, 'SBobby@hotmail.com', 49.87, 1234.32, 'notes2');
INSERT INTO `mydb`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cust_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'LeBron', 'James', '4311 Star Ave.', 'Los Angeles', 'CA', 90210, 3423423140, 'LBJ@nike.com', 4821.03, 321.32, 'notes3');
INSERT INTO `mydb`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cust_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Dwyane', 'Wade', '808 Penny Lane', 'Miami', 'FL', 33214, 3058342923, 'DWade@aol.com', 342.90, 90.23, 'notes4');
INSERT INTO `mydb`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cust_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Sally', 'Smith', '3421 Orange Road', 'Bolder', 'CO', 23410, 9034128412, 'SallySmith@yahoo.com', 32.12, 423.12, 'notes5');
INSERT INTO `mydb`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cust_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Herold', 'Grace', '323 Oceans Avenue', 'Tallahassee', 'FL', 23412, 4234132321, 'Heroldlikespets@yahoo.com', 78.25, 34.23, 'notes6');
INSERT INTO `mydb`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cust_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Charles', 'Downey', '853 Island Avenue', 'Austin', 'TX', 34281, 4238953201, 'CharlesDowney@aol.com', 481, 968.03, 'notes8');
INSERT INTO `mydb`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cust_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Michael', 'Demaio', '4372 Franklin Circle', 'Honolulu', 'HW', 56932, 7894153124, 'mdemaio@facebook.com', 582, 99.04, 'notes9');
INSERT INTO `mydb`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cust_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Julian', 'Paredes', '101 Bourbon St.', 'Louisiana', 'NO', 42892, 4893291203, 'julian.paredes@aim.com', 9, 10.05, 'notes10');
INSERT INTO `mydb`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cust_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Grace', 'Johnson', '1095 Travis Road', 'Houston', 'TX', 68432, 9854350655, 'JohnsonAndJohnson@gmail.com', 48.21, 583.00, 'notes7');
INSERT INTO `mydb`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cust_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Davud', 'Dautbasic', '446 Conradi St', 'Tallahassee', 'FL', 32304, 7273126622, 'dd17h@my.fsu.edu', 100, 100, 'notes1');

COMMIT;


-- -----------------------------------------------------
-- Data for table `mydb`.`pet`
-- -----------------------------------------------------
START TRANSACTION;
USE `mydb`;
INSERT INTO `mydb`.`pet` (`pet_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (1, 1, 'Doberman', 'm', 350, 525, 52, 'black/tan', 'y', 'y', 'notes1');
INSERT INTO `mydb`.`pet` (`pet_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (2, 2, 'Chiwawa', 'f', 175, 165, 78, 'white/brown', 'n', 'n', 'notes2');
INSERT INTO `mydb`.`pet` (`pet_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (3, 3, 'American Bobtail', 'm', 195, 123, 23, 'black', 'n', 'n', 'notes3');
INSERT INTO `mydb`.`pet` (`pet_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (4, 4, 'Pitbull', 'f', 300, 321, 12, 'white', 'y', 'n', 'notes4');
INSERT INTO `mydb`.`pet` (`pet_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (5, 5, 'Giraffe', 'f', 5000, 4300, 30, 'orange', 'y', 'y', 'notes5');
INSERT INTO `mydb`.`pet` (`pet_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (6, 6, 'African Gray Parrot', 'f', 234, 523, 23, 'rainbow', 'y', 'n', 'notes6');
INSERT INTO `mydb`.`pet` (`pet_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (7, 7, 'Rattle Snake', 'm', 800, 900, 3, 'green tan', 'n', 'n', 'notes7');
INSERT INTO `mydb`.`pet` (`pet_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (8, 8, 'Berneese Mountain Dog', 'f', 320, 600, 40, 'black brown white', 'y', 'y', 'notes8');
INSERT INTO `mydb`.`pet` (`pet_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (9, 9, 'Greyhound', 'm', 200, 200, 12, 'gray white', 'n', 'y', 'notes9');
INSERT INTO `mydb`.`pet` (`pet_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (10, 10, 'Monkey', 'm', 5000, 6000, 20, 'brown', 'y', 'y', 'notes10');

COMMIT;


