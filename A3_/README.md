> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

LIS4381 - Mobile Applcation Development

Davud Dautbasic

Assignment 3 Requirements:

*Sub-Heading:*

1. Create an app that calculates ticket prices
2. Screenshots of Applications
3. ERD screenshots
4. Bitbucket repository links

#### README.md file should include the following items:

* Screenshots of Application

#### Assignment Links:

*A3 sql code*
[A3 SQL Link](a3script.sql "A3 SQL")

*A3 ERD stuff*
[A3 ERD Link](a3erd.mwb "A3 ERD")

#### Assignment Screenshots:

*Screenshot of 1st page of App:

![picture](images/screen1.PNG)

*Screenshot of 2nd page of App*:

![picture](images/screen2.PNG)

*Screenshot of ERD:
![picture](images/erd.PNG)



#### Tutorial Links:


*A3 lis4381 Repository:*
[A3 Repository Link](https://bitbucket.org/davuddautbasic/lis4381/src/master/A3/ "lis4381 Repository")